#pragma once
#include "Runic.h"
#include "Editor/Selectable.h"
namespace Runic {
	class ItemSelectedEvent : public Event
	{
	public:
		ItemSelectedEvent(const Selectable& e) : m_Item(e){}
		inline Selectable GetItem() const { return m_Item; }
		
		std::string ToString() const override
		{
			std::stringstream ss;
			ss << "EntitySelectedEvent: " ;
			return ss.str();
		}

		EVENT_CLASS_TYPE(None)
			EVENT_CLASS_CATEGORY(EventCategoryApplication)

	private:
		Selectable m_Item;
	};


}