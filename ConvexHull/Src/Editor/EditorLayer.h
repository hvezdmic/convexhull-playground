#pragma once

#include "Runic.h"
#include "Panels/Panel.h"
#include <filesystem>

// Main layer of the application
// It takes care of all panels and distributes update calls and events
namespace Runic {
	class EditorLayer : public Layer
	{
	public:
		EditorLayer();
		virtual ~EditorLayer() = default;
		virtual void OnAttach() override;
		virtual void OnDetach() override;
		
		void OnUpdate(DeltaTime ts) override;
		virtual void OnImGuiRender() override;
		void OnEvent(Event& e) override;
	
	private:
		
		std::unordered_map<std::string, Ref<Panel>> m_Panels;
		Ref<Scene> m_ActiveScene;
	};

}

