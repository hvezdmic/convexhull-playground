#pragma once
#include "Runic.h"

#include "Panel.h"



namespace Runic {
	// Viewport panel of the application
	class EditorViewport : public Panel {

	public:
		EditorViewport();
		virtual void OnEvent(Event& e) override;

		// Here is the computation of the jarvis algorithm for convex hull
		virtual void OnUpdate(DeltaTime ts) override;
		virtual void OnRender() override;
	private:
		/// Draws the contents of the panel such as background and lines between convex hull points 
		void Draw();
		// Updates it's state if an entity was changed
		bool OnEntityChanged(EntityChangedEvent);
		// Updates it's state if an entity was added
		bool OnEntityAdded(EntityAddedEvent);
		// Updates it's state if an entity was remoded
		bool OnEntityRemoved(EntityRemovedEvent);

	private:
		//entity id of the hovered point
		uint32_t m_Hovered_point;
		// list of convex hull points to be connected
		std::vector<glm::vec3>					m_Line_loop;
		Runic::Ref<Scene>						m_Scene;
		bool							m_Focused = false;
		bool							m_Clicked = false;
		// should the convex hull be reconstructed
		bool							m_ReconstructHull;

	};


}

