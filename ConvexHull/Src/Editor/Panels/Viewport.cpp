#include "Viewport.h"
#include <glm/gtc/matrix_transform.hpp>
#include <imgui/imgui.h>
#include "Runic.h"


#include <limits>

Runic::EditorViewport::EditorViewport() : m_Scene(SceneManager::GetActiveScene()), Panel("Scene")
{
}

void Runic::EditorViewport::OnEvent(Event & e)
{
    EventDispatcher dispatcher(e);
    dispatcher.Dispatch<EntityAddedEvent>(RUNIC_BIND_EVENT_FN(EditorViewport::OnEntityAdded));
    dispatcher.Dispatch<EntityChangedEvent>(RUNIC_BIND_EVENT_FN(EditorViewport::OnEntityChanged));
    dispatcher.Dispatch<EntityRemovedEvent>(RUNIC_BIND_EVENT_FN(EditorViewport::OnEntityRemoved));
}


// Update state of the panel
// Compute convex hull using Jarvis algorithm
void Runic::EditorViewport::OnUpdate(DeltaTime ts)
{
    if (!m_ReconstructHull) return;
    std::vector<glm::vec3> points;
    m_Line_loop.clear();
    // Get all points from scene
    m_Scene->view<TransformComponent>().each([&points](auto& e, auto& t) mutable {
        points.push_back(t.transform.Position());
    });

    glm::vec3 leftmost;
    float min = FLT_MAX;
    // find the left most point
    for (auto& p : points) {
        if (p.x < min) {
            leftmost = p;
            min = p.x;
        }
    }
    if (!points.size())return;
    glm::vec3 endPoint;
    do {
        m_Line_loop.push_back(leftmost);
        endPoint = points.front();
        for (auto& p : points) {
            glm::vec3 last_to_endpoint = endPoint - m_Line_loop.back();
            glm::vec3 last_to_point = p - m_Line_loop.back();
            
            glm::vec3 C = glm::cross(last_to_endpoint, last_to_point);
            float d = glm::dot(C, glm::vec3(0, 0, 1));
            // Check the angle between vectors last_to_endpoint and last_to_point
            if (endPoint == leftmost || d > 0)
                endPoint = p; // found greater left turn, update endpoint
        }
        leftmost = endPoint;
    } while (endPoint != m_Line_loop.front());
    m_ReconstructHull = false;
}

void Runic::EditorViewport::OnRender()
{
	Draw();
}

void Runic::EditorViewport::Draw()
{
	ImGuiWindowFlags window_flags = 0;
    
	window_flags |=  ImGuiWindowFlags_NoCollapse | ImGuiWindowFlags_NoScrollbar | ImGuiWindowFlags_NoTitleBar;
	
    if (!ImGui::BeginChild(m_Name.c_str(),ImVec2(0,0), window_flags)) {
        ImGui::EndChild();
        return;
    }
    float windowHeight = Runic::Application::Get().GetWindow().GetHeight();
    float imguiwindowHeight = ImGui::GetWindowSize().y;

    ImDrawList* draw_list = ImGui::GetWindowDrawList();

    static bool adding_point = false;
    bool clear = ImGui::Button("Clear");

   
    ImGui::Text("Left-click and drag to add a point,\nRight-click to remove point");
    ImVec2 canvas_size = ImGui::GetContentRegionAvail();        // Resize canvas to what's available
    ImVec2 canvas_pos = ImGui::GetCursorScreenPos();          
    ImVec2  canvas_start = canvas_pos;


    draw_list->AddRectFilledMultiColor(canvas_pos, ImVec2(canvas_pos.x + canvas_size.x, canvas_pos.y + canvas_size.y), IM_COL32(50, 50, 50, 255), IM_COL32(50, 50, 60, 255), IM_COL32(60, 60, 70, 255), IM_COL32(50, 50, 60, 255));
    draw_list->AddRect(canvas_pos, ImVec2(canvas_pos.x + canvas_size.x, canvas_pos.y + canvas_size.y), IM_COL32(255, 255, 255, 255));

    bool adding_preview = false;
    bool static PointHovered = false;
    //ImGui::InvisibleButton("canvas", canvas_size);
    ImVec2 mouse_pos_in_canvas = ImVec2(ImGui::GetIO().MousePos.x - canvas_pos.x, ImGui::GetIO().MousePos.y - canvas_pos.y);
    
    glm::mat4 m_viewPort = glm::mat4(1,0.0f, 0.0f, 0.0f,
                                     0.0f, 1, 0.0f, 0.0f,
                                     0,0,1,0,
                                     canvas_pos.x, canvas_pos.y, 0.0f, 1.0f);
    static bool pointDrag = false;
    Entity e;
    float sz = 15.0f;
    float button_size = sz * 1.8;
    if (adding_point) {
        e = m_Scene->CreateEntity("Point");
        auto& t = e.GetComponent<TransformComponent>();
        t.transform.SetPosition(glm::vec3(mouse_pos_in_canvas.x, mouse_pos_in_canvas.y, 0.0f));
        adding_point = false;
        pointDrag = true;
        EntityAddedEvent evnt(e);
        Runic::Application::Get().OnEvent(evnt);
        RUNIC_INFO("POINT ADDED: ({0},{1})", mouse_pos_in_canvas.x, mouse_pos_in_canvas.y);
    }
    draw_list->PushClipRect(canvas_pos, ImVec2(canvas_pos.x + canvas_size.x, canvas_pos.y + canvas_size.y), true);    

    for (uint32_t i = 0; i < m_Line_loop.size(); i++) {
        glm::vec4 p1 = m_viewPort * glm::vec4(m_Line_loop[i].x, m_Line_loop[i].y, 0.0f, 1.0f);
        glm::vec4 p2 = m_viewPort * glm::vec4(m_Line_loop[(i + 1) % m_Line_loop.size()].x, m_Line_loop[(i + 1) % m_Line_loop.size()].y, 0.0f, 1.0f);
        
        draw_list->AddLine(ImVec2(p1.x, p1.y), ImVec2(p2.x, p2.y), IM_COL32(176, 56, 77, 200), 10.0f);
    }
    m_Scene->view<TransformComponent>().each([&](auto& e, auto& transformComponent) {
        glm::vec4 screen_point = m_viewPort * glm::vec4(transformComponent.transform.Position(), 1.0f);
        ImGui::BeginGroup();
        ImGui::SetCursorPos(ImVec2(transformComponent.transform.Position().x - button_size * 0.35, transformComponent.transform.Position().y + button_size * 1.7f));

        if (ImGui::InvisibleButton(("##" + std::to_string((uint32_t) e)).c_str(), ImVec2(button_size, button_size))) {
            
        }
        if (ImGui::IsItemHovered())
        {
            PointHovered = true;
            m_Hovered_point = (uint32_t)e;

            if (ImGui::IsItemClicked(ImGuiMouseButton_Left))
            {
                pointDrag = true;
            }
            if (ImGui::IsItemClicked(ImGuiMouseButton_Right)) {
                EntityRemovedEvent evnt(m_Scene->GetEntity(e));
                Runic::Application::Get().OnEvent(evnt);
                RUNIC_INFO("POINT Removed: ({0},{1})", transformComponent.transform.Position().x, transformComponent.transform.Position().y);
                m_Scene->RemoveEntity(m_Scene->GetEntity(e));
                PointHovered = false;
            }
        }
        else { PointHovered = false; }
        draw_list->AddCircleFilled(ImVec2(screen_point.x, screen_point.y), (uint32_t)e == m_Hovered_point && PointHovered ? sz * 1.1 : sz, IM_COL32(255, 255, 255, 255), 24);
        ImGui::EndGroup();    
    });

    if (pointDrag && ImGui::IsMouseDown(ImGuiMouseButton_Left)) {
        Entity ent = m_Scene->GetEntity((entt::entity)m_Hovered_point);
        if (ent.IsValid()) {
            auto& t = ent.GetComponent<TransformComponent>();
            t.transform.SetPosition(glm::vec3(std::min(std::max(mouse_pos_in_canvas.x, 10.0f), canvas_size.x - 10), std::min(std::max(mouse_pos_in_canvas.y, 10.0f), canvas_size.y - 10), 1.0f));
            EntityChangedEvent evnt(e);
            Runic::Application::Get().OnEvent(evnt);
        }
    }
    else pointDrag = false;

    if (ImGui::IsWindowHovered())
    {
        if (!PointHovered && !adding_point && ImGui::IsMouseClicked(0) && mouse_pos_in_canvas.x > 0 &&
            mouse_pos_in_canvas.x < canvas_size.x &&
            mouse_pos_in_canvas.y > 0 &&
            mouse_pos_in_canvas.y < canvas_size.y)
        {
            adding_point = true;
        }
    }
    for (uint32_t i = 0; i < m_Line_loop.size(); i++) {
        glm::vec4 p1 = m_viewPort * glm::vec4(m_Line_loop[i].x, m_Line_loop[i].y, 0.0f, 1.0f);
        draw_list->AddCircleFilled(ImVec2(p1.x, p1.y), (sz * 0.5), IM_COL32(176, 56, 77, 200), 24);
    }
    draw_list->PopClipRect();
    if (clear) {
        m_Scene->view<TransformComponent>().each([&](auto& e, auto& transformComponent) {
            Entity ent =  m_Scene->GetEntity(e);
            if (ent.IsValid()) {
                EntityRemovedEvent evnt(m_Scene->GetEntity(e));
                Runic::Application::Get().OnEvent(evnt);
                m_Scene->RemoveEntity(m_Scene->GetEntity(e));
                RUNIC_INFO("POINT Removed: ({0},{1})", transformComponent.transform.Position().x, transformComponent.transform.Position().y);
            }
        });
    }
	ImGui::EndChild();
}

bool Runic::EditorViewport::OnEntityChanged(EntityChangedEvent)
{

    m_ReconstructHull = true;
    return false;
}

bool Runic::EditorViewport::OnEntityAdded(EntityAddedEvent)
{
    m_ReconstructHull = true;
    return false;
}

bool Runic::EditorViewport::OnEntityRemoved(EntityRemovedEvent)
{
    m_ReconstructHull = true;
    return false;
}
