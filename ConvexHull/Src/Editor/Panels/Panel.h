#pragma once
#include "Runic.h"
#include <string>
#include "Panel.h"
namespace Runic {
	// Parent abstract class for panels
	class Panel
	{
	public:
		Panel(const std::string& name = "Panel");
		virtual ~Panel();

		virtual void OnUpdate(DeltaTime ts) = 0;
		virtual void OnEvent(Event& e) = 0;
		virtual void OnRender() = 0;
		inline const std::string& GetName() const { return m_Name; }
		inline void Show() { m_Show = true; };
	protected:
		std::string m_Name;
		bool		m_Show= true;
	};

}