#include "EditorLayer.h"
#include <imgui/imgui.h>
#include <iostream>
#include <glm/glm.hpp>
#include "Panels/Viewport.h"
#include <fstream>
#include <sstream>
#include <string>
//-------------------------------------------------------------------
// Autor: Michal Hvezda
// Contact: Hvezdmic@fel.cvut.cz
//-------------------------------------------------------------------




namespace Runic {





	EditorLayer::EditorLayer()
		: Layer("EditorLayer")
	{

	}

	void EditorLayer::OnImGuiRender()
	{
		
		static bool dockspaceOpen = true;
		static ImGuiDockNodeFlags dockspace_flags = ImGuiDockNodeFlags_None;
		static bool opt_fullscreen_persistant = true;
		static bool show_log = true;
		static bool show_console = true;

		bool opt_fullscreen = opt_fullscreen_persistant;

		ImGuiWindowFlags window_flags = ImGuiWindowFlags_MenuBar | ImGuiWindowFlags_NoDocking;
		if (opt_fullscreen)
		{
			ImGuiViewport* viewport = ImGui::GetMainViewport();
			ImGui::SetNextWindowPos(viewport->Pos);
			ImGui::SetNextWindowSize(viewport->Size);
			ImGui::SetNextWindowViewport(viewport->ID);
			ImGui::PushStyleVar(ImGuiStyleVar_WindowRounding, 0.0f);
			ImGui::PushStyleVar(ImGuiStyleVar_WindowBorderSize, 0.0f);
			window_flags |= ImGuiWindowFlags_NoTitleBar | ImGuiWindowFlags_NoCollapse | ImGuiWindowFlags_NoResize | ImGuiWindowFlags_NoMove;
			window_flags |= ImGuiWindowFlags_NoBringToFrontOnFocus | ImGuiWindowFlags_NoNavFocus;
		}

		if (dockspace_flags & ImGuiDockNodeFlags_PassthruCentralNode)
			window_flags |= ImGuiWindowFlags_NoBackground;
		ImGui::PushStyleVar(ImGuiStyleVar_WindowPadding, ImVec2(0.0f, 0.0f));
		ImGui::Begin("DockSpace", &dockspaceOpen, window_flags);
		ImGui::PopStyleVar();

		if (opt_fullscreen)
			ImGui::PopStyleVar(2);

		// DockSpace
		ImGuiIO& io = ImGui::GetIO();
		if (io.ConfigFlags & ImGuiConfigFlags_DockingEnable)
		{
			ImGuiID dockspace_id = ImGui::GetID("MyDockSpace");
			ImGui::DockSpace(dockspace_id, ImVec2(0.0f, 0.0f), dockspace_flags);
		}

		for (auto [_, p] : m_Panels)
			p->OnRender();
		
		ImGui::End();


		
	}

	void EditorLayer::OnAttach()
	{
		m_ActiveScene = CreateRef<Scene>();
		SceneManager::SetActiveScene(m_ActiveScene);
		// Using Working directory as input file directory.
		auto inputFile = FileSystem::GetWorkingDirectory();
		if (!inputFile.empty()) {
			if (FileSystem::Exists(inputFile)) {
				if (inputFile.has_filename()) {
					std::ifstream in(inputFile);
					//in.open(inputFile);
					if (in.is_open()) {
						std::string line;
						while (std::getline(in, line))
						{
							std::istringstream iss(line);
							float x, y;
							if (!(iss >> x >> y)) { break; } // error
							Entity e = m_ActiveScene->CreateEntity("Point");
							auto& t = e.GetComponent<TransformComponent>();
							t.transform.SetPosition(glm::vec3(x, y, 0.0f));
						}
						in.close();
					}
					else { RUNIC_ERROR("Failed to open {0}", inputFile); }
				}
				else { RUNIC_WARN("File {0} is not a file", inputFile); }
			}
			else { RUNIC_WARN("File {0} not found", inputFile); }
		}



		m_Panels["Scene"] = CreateRef<EditorViewport>();







		//m_Panels["Log"] = (CreateRef<EditorAppLog>());
		
	}

	void EditorLayer::OnDetach()
	{

	}

	void EditorLayer::OnUpdate(DeltaTime ts)
	{
		for (auto [_, p] : m_Panels)
			p->OnUpdate(ts);


	}

	void EditorLayer::OnEvent(Runic::Event & e)
	{
		for (auto[_,p] : m_Panels)
			p->OnEvent(e);

	}
}