#pragma once
#include "Runic.h"

namespace Runic {
	enum RuneForgeSelectableFlags {
		RuneForgeSelectableFlags_ =				BIT(0),
		RuneForgeSelectableFlags_File =			BIT(1),
		RuneForgeSelectableFlags_Shader =		BIT(2),
		RuneForgeSelectableFlags_Mesh =			BIT(3),
		RuneForgeSelectableFlags_Texture =		BIT(4),
		RuneForgeSelectableFlags_Material =		BIT(5),
		RuneForgeSelectableFlags_Entity =		BIT(6),
	};
	struct Selectable {
		RuneForgeSelectableFlags flags;
		uint32_t					ID;
		operator entt::entity() {
			return (entt::entity)ID;
		}
	};


}