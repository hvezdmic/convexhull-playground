#include <Runic.h>
#include <Runic/Core/EntryPoint.h>

#include "EditorLayer.h"
//-------------------------------------------------------------------
// Autor: Michal Hvezda
// Contact: Hvezdmic@fel.cvut.cz
// 
//-------------------------------------------------------------------


//Entry point of the client application
//Its is called within main function withic runic engine entry point
namespace Runic
{
	class ConvexHull : public Runic::Application
	{
	public:
		ConvexHull() : Application("ConvexHull Playground")
		{
			Application::PushLayer(new EditorLayer());
		}
		~ConvexHull() {}
	};
}
Runic::Application* Runic::CreateApplication(int argc, char* argv[]) {
	if (argc > 1) {




		RUNIC_CORE_INFO("Argument: {0}", argv[1]);

		std::string arg = argv[1];
		if (arg == "--help") {
			RUNIC_INFO(R"(
ConvexHull Playground is a graphical application that connects points on the screen to a convex hull!
Controls:
	left mouse click:	add a point
	left mouse click and drag: move a point
	right mouse click:	remove a point

\nConsole aguments:
	app takes only one argument which is to path to input file.
	Input file data must be as follows: <X coordinate> <Y coordinate> 
)");
			return nullptr;
		}
		FileSystem::SetWorkingDirectory(arg);
	}




	return new ConvexHull();
}