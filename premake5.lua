workspace "ConvexHull"
	architecture "x64"
	startproject "ConvexHull"
	configurations
	{
		"Debug",
		"Release",
		"Dist"
	}

outputdir = "%{cfg.buildcfg}-%{cfg.system}-%{cfg.architecture}"

IncludeDir = {}
IncludeDir["GLFW"] = "Runic/External/GLFW/include"
IncludeDir["Glad"] = "Runic/External/Glad/include"
IncludeDir["imgui"] = "Runic/External/imgui"
IncludeDir["glm"] = "Runic/External/glm"
IncludeDir["entt"] = "Runic/External/EnTT/include"
IncludeDir["objloader"] = "Runic/External/OBJLoader/include"

include "Runic/External/GLFW"
include "Runic/External/Glad"
include "Runic/External/imgui"


project "Runic"
	location "Runic"
	kind "StaticLib"
	staticruntime "On"
	
	language "C++"
	cppdialect "C++Latest"

	targetdir ("bin/" .. outputdir .. "/%{prj.name}")
	objdir ("bin-int/" .. outputdir .. "/%{prj.name}")


	pchheader "runicpch.h"
	pchsource "Runic/Src/runicpch.cpp"
	defines{
		"_CRT_SECURE_NO_WARNINGS",
		"NOMINMAX"
	}
	files
	{
	"%{prj.name}/Src/**.h",
	"%{prj.name}/Src/**.cpp",
	"%{prj.name}/External/glm/glm/**.hpp",
	"%{prj.name}/External/glm/glm/**.inl"
	}

	includedirs
	{
	"%{prj.name}/Src",
	"%{prj.name}/External/spdlog/include",
	"%{IncludeDir.GLFW}",
	"%{IncludeDir.Glad}",
	"%{IncludeDir.imgui}",
	"%{IncludeDir.glm}",
	"%{IncludeDir.entt}",
	"%{IncludeDir.objloader}"
	}
	
	links
	{
		"GLFW",
		"Glad",
		"opengl32.lib",
		"imgui"
	}

	filter "system:windows"
		systemversion "latest"

		defines{
			"RUNIC_PLATFORM_WINDOWS",
			"RUNIC_BUILD_DLL",
			"GLFW_INCLUDE_NONE",
			"_CRT_SECURE_NO_WARNINGS"
		}

		
	filter "configurations:Debug"
		defines "RUNIC_DEBUG"
		buildoptions "/MDd"
		symbols "on"

	filter "configurations:Release"
		defines "RUNIC_RELEASE"
		buildoptions "/MD"
		optimize "on"

	filter "configurations:Dist"
		defines "RUNIC_DIST"
		buildoptions "/MD"
		optimize "on"

project "ConvexHull"
	location "ConvexHull"
	kind "ConsoleApp"
	staticruntime "On"
	
	language "C++"
	cppdialect "C++Latest"

	targetdir ("bin/" .. outputdir .. "/%{prj.name}")
	objdir ("bin-int/" .. outputdir .. "/%{prj.name}")
	
	files
	{
	"%{prj.name}/Src/**.h",
	"%{prj.name}/Src/**.cpp"
	}

	includedirs
	{
	"%{prj.name}/Src",
	"Runic/External/spdlog/include",
	"Runic/Src",
	"Runic/External",
	"%{IncludeDir.glm}",
	"%{IncludeDir.Glad}",
	"%{IncludeDir.entt}",
	"%{IncludeDir.objloader}"
	}
	links 
	{
		"Runic"	
	}

	filter "system:windows"
		systemversion "latest"

		defines{
			"RUNIC_PLATFORM_WINDOWS","NOMINMAX"

		}

	filter "configurations:Debug"
		defines {"RUNIC_DEBUG", "RUNIC_ENABLE_ASSERT"}
		buildoptions "/MDd"
		symbols "On"

	filter "configurations:Release"
		defines "RUNIC_RELEASE"
		buildoptions "/MD"
		optimize "On"

	filter "configurations:Dist"
		defines "RUNIC_DIST"
		buildoptions "/MD"
		optimize "On"