#pragma once
#include "runicpch.h"
#include<list>
namespace Runic {
	struct LogBuffer
	{
		LogBuffer() : m_max_size(1000)
		{
			
		}
		LogBuffer(unsigned max_size) : m_max_size(max_size)
		{
		}
		inline const std::list<std::string>& data() { return m_data; }
		void clear() { m_data.clear(); }
		void append(std::string& other) {
			if (m_data.size() < m_max_size)m_data.push_back(other);
			else { m_data.pop_front(); m_data.push_back(other);
			}
		}
		inline size_t size() { return m_data.size(); }
		inline size_t max_size() { return m_max_size; }
		void set_max_size(size_t n) { m_max_size = n; }

	private:
		size_t m_max_size;
		std::list<std::string> m_data;
	};
}