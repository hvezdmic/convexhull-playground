#pragma once

/* Printable keys */
#define RUNIC_KEY_SPACE              32
#define RUNIC_KEY_APOSTROPHE         39  /* ' */
#define RUNIC_KEY_COMMA              44  /* , */
#define RUNIC_KEY_MINUS              45  /* - */
#define RUNIC_KEY_PERIOD             46  /* . */
#define RUNIC_KEY_SLASH              47  /* / */
#define RUNIC_KEY_0                  48
#define RUNIC_KEY_1                  49
#define RUNIC_KEY_2                  50
#define RUNIC_KEY_3                  51
#define RUNIC_KEY_4                  52
#define RUNIC_KEY_5                  53
#define RUNIC_KEY_6                  54
#define RUNIC_KEY_7                  55
#define RUNIC_KEY_8                  56
#define RUNIC_KEY_9                  57
#define RUNIC_KEY_SEMICOLON          59  /* ; */
#define RUNIC_KEY_EQUAL              61  /* = */
#define RUNIC_KEY_A                  65
#define RUNIC_KEY_B                  66
#define RUNIC_KEY_C                  67
#define RUNIC_KEY_D                  68
#define RUNIC_KEY_E                  69
#define RUNIC_KEY_F                  70
#define RUNIC_KEY_G                  71
#define RUNIC_KEY_H                  72
#define RUNIC_KEY_I                  73
#define RUNIC_KEY_J                  74
#define RUNIC_KEY_K                  75
#define RUNIC_KEY_L                  76
#define RUNIC_KEY_M                  77
#define RUNIC_KEY_N                  78
#define RUNIC_KEY_O                  79
#define RUNIC_KEY_P                  80
#define RUNIC_KEY_Q                  81
#define RUNIC_KEY_R                  82
#define RUNIC_KEY_S                  83
#define RUNIC_KEY_T                  84
#define RUNIC_KEY_U                  85
#define RUNIC_KEY_V                  86
#define RUNIC_KEY_W                  87
#define RUNIC_KEY_X                  88
#define RUNIC_KEY_Y                  89
#define RUNIC_KEY_Z                  90
#define RUNIC_KEY_LEFT_BRACKET       91  /* [ */
#define RUNIC_KEY_BACKSLASH          92  /* \ */
#define RUNIC_KEY_RIGHT_BRACKET      93  /* ] */
#define RUNIC_KEY_GRAVE_ACCENT       96  /* ` */
#define RUNIC_KEY_WORLD_1            161 /* non-US #1 */
#define RUNIC_KEY_WORLD_2            162 /* non-US #2 */

/* Function keys */
#define RUNIC_KEY_ESCAPE             256
#define RUNIC_KEY_ENTER              257
#define RUNIC_KEY_TAB                258
#define RUNIC_KEY_BACKSPACE          259
#define RUNIC_KEY_INSERT             260
#define RUNIC_KEY_DELETE             261
#define RUNIC_KEY_RIGHT              262
#define RUNIC_KEY_LEFT               263
#define RUNIC_KEY_DOWN               264
#define RUNIC_KEY_UP                 265
#define RUNIC_KEY_PAGE_UP            266
#define RUNIC_KEY_PAGE_DOWN          267
#define RUNIC_KEY_HOME               268
#define RUNIC_KEY_END                269
#define RUNIC_KEY_CAPS_LOCK          280
#define RUNIC_KEY_SCROLL_LOCK        281
#define RUNIC_KEY_NUM_LOCK           282
#define RUNIC_KEY_PRINT_SCREEN       283
#define RUNIC_KEY_PAUSE              284
#define RUNIC_KEY_F1                 290
#define RUNIC_KEY_F2                 291
#define RUNIC_KEY_F3                 292
#define RUNIC_KEY_F4                 293
#define RUNIC_KEY_F5                 294
#define RUNIC_KEY_F6                 295
#define RUNIC_KEY_F7                 296
#define RUNIC_KEY_F8                 297
#define RUNIC_KEY_F9                 298
#define RUNIC_KEY_F10                299
#define RUNIC_KEY_F11                300
#define RUNIC_KEY_F12                301
#define RUNIC_KEY_F13                302
#define RUNIC_KEY_F14                303
#define RUNIC_KEY_F15                304
#define RUNIC_KEY_F16                305
#define RUNIC_KEY_F17                306
#define RUNIC_KEY_F18                307
#define RUNIC_KEY_F19                308
#define RUNIC_KEY_F20                309
#define RUNIC_KEY_F21                310
#define RUNIC_KEY_F22                311
#define RUNIC_KEY_F23                312
#define RUNIC_KEY_F24                313
#define RUNIC_KEY_F25                314
#define RUNIC_KEY_KP_0               320
#define RUNIC_KEY_KP_1               321
#define RUNIC_KEY_KP_2               322
#define RUNIC_KEY_KP_3               323
#define RUNIC_KEY_KP_4               324
#define RUNIC_KEY_KP_5               325
#define RUNIC_KEY_KP_6               326
#define RUNIC_KEY_KP_7               327
#define RUNIC_KEY_KP_8               328
#define RUNIC_KEY_KP_9               329
#define RUNIC_KEY_KP_DECIMAL         330
#define RUNIC_KEY_KP_DIVIDE          331
#define RUNIC_KEY_KP_MULTIPLY        332
#define RUNIC_KEY_KP_SUBTRACT        333
#define RUNIC_KEY_KP_ADD             334
#define RUNIC_KEY_KP_ENTER           335
#define RUNIC_KEY_KP_EQUAL           336
#define RUNIC_KEY_LEFT_SHIFT         340
#define RUNIC_KEY_LEFT_CONTROL       341
#define RUNIC_KEY_LEFT_ALT           342
#define RUNIC_KEY_LEFT_SUPER         343
#define RUNIC_KEY_RIGHT_SHIFT        344
#define RUNIC_KEY_RIGHT_CONTROL      345
#define RUNIC_KEY_RIGHT_ALT          346
#define RUNIC_KEY_RIGHT_SUPER        347
#define RUNIC_KEY_MENU               348
