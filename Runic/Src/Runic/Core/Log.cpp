#include "runicpch.h"
#include "Log.h"
#include "spdlog/sinks/dist_sink.h"
#include "spdlog/sinks/stdout_color_sinks.h"
#include "spdlog/sinks/basic_file_sink.h"
#include "spdlog/sinks/base_sink.h"




namespace Runic {

	template<typename Mutex>
	class Buffer_sink : public spdlog::sinks::base_sink <Mutex>
	{
	public:
		Buffer_sink(LogBuffer& stream) : m_buffer(stream)
		{
		}
	
	private:
		LogBuffer& m_buffer;
	protected:
		void sink_it_(const spdlog::details::log_msg& msg) override
		{
	
			// log_msg is a struct containing the log entry info like level, timestamp, thread id etc.
			// msg.raw contains pre formatted log
			
			// If needed (very likely but not mandatory), the sink formats the message before sending it to its final destination:
			spdlog::memory_buf_t formatted;
			base_sink<Mutex>::formatter_->format(msg, formatted);
			m_buffer.append(fmt::to_string(formatted));
		}
	
		void flush_() override
		{
			std::cout << std::flush;
		}
	};

	std::shared_ptr<spdlog::logger> Log::s_CoreLogger;
	std::shared_ptr<spdlog::logger> Log::s_ClientLogger;
	std::shared_ptr<LogBuffer> Log::s_Buffer;

	void Log::Init()
	{
		s_Buffer = std::make_shared<LogBuffer>();
		auto dist_sink = std::make_shared<spdlog::sinks::dist_sink_st>();
		auto sink1 = std::make_shared<spdlog::sinks::stdout_color_sink_st>();
		auto sink2 = std::make_shared<spdlog::sinks::basic_file_sink_st>("Log.log");
		auto sink3 = std::make_shared<Buffer_sink<spdlog::details::null_mutex>>(*s_Buffer);
		sink1->set_pattern("%^[%T] %n: %v%$");
		dist_sink->add_sink(sink1);
		dist_sink->add_sink(sink2);
		dist_sink->add_sink(sink3);
		
		
		s_CoreLogger = std::make_shared<spdlog::logger>("RUNIC", dist_sink);
		s_CoreLogger->set_level(spdlog::level::trace);
		s_ClientLogger = std::make_shared<spdlog::logger>("APP", dist_sink);
		s_ClientLogger->set_level(spdlog::level::trace);
		spdlog::flush_every(std::chrono::seconds(5));
		spdlog::set_default_logger(s_ClientLogger);

	}
}
