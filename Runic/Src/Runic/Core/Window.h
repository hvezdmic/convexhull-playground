#pragma once
#include "runicpch.h"

#include "Runic/Core/Core.h"
#include "Runic/Events/Event.h"
#include "Runic/Core/TimeStep.h"


namespace Runic {
	struct WindowProps
	{
		std::string Title;
		unsigned int Width;
		unsigned int Height;

		WindowProps(const std::string& title = "Project Runic",
					unsigned int width = 1920,
					unsigned int height = 1080
					)
			: Title(title), Width(width), Height(height){}

	};
	class RUNIC_API Window
	{
	public:
		using EventCallbackFn = std::function<void(Event&)>;
		virtual ~Window(){}
		virtual void OnUpdate(DeltaTime ts) = 0;
		virtual unsigned int GetWidth() const = 0;
		virtual unsigned int GetHeight() const = 0;


		virtual void SetEventCallback(const EventCallbackFn& callback) = 0;
		virtual void SetVSync(bool Enabled) = 0;
		virtual bool IsVSync() const = 0;

		virtual void* GetNativeWindow() const = 0;

		static Window* Create(const WindowProps& props = WindowProps());
	};
}