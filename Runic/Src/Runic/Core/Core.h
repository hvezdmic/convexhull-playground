#pragma once
#include <memory>

#ifdef RUNIC_PLATFORM_WINDOWS
	#if RUNIC_DYNAMIC_LINK
		#ifdef RUNIC_BUILD_DLL
			#define RUNIC_API __declspec(dllexport)
		#else
			#define RUNIC_API __declspec(dllimport)
		#endif // RN_BUILD_DLL
#else 
	#define RUNIC_API
#endif
#else 
	#error RUNIC IS WINDOWS APPLICATION

#endif // RN_PLATFORTM_WINDOWS


#ifdef RUNIC_DEBUG
#define RUNIC_ENABLE_ASSERT
#endif // RUNIC_DEBUG



#ifdef RUNIC_ENABLE_ASSERT
	#define RUNIC_ASSERT(x, ...) {if(!(x)){RUNIC_ERROR("Assertion Failed: {0}", __VA_ARGS__); __debugbreak();}} 
	#define RUNIC_CORE_ASSERT(x, ...) {if(!(x)){RUNIC_CORE_ERROR("Assertion Failed: {0}", __VA_ARGS__); __debugbreak();}} 
#else
	#define RUNIC_ASSERT(x,...)
	#define RUNIC_CORE_ASSERT(x,...)
#endif // RUNIC_ENABLE_ASSERT

#define BIT(x) (1 << x)
#define RUNIC_BIND_EVENT_FN(x) std::bind(&x, this, std::placeholders::_1)

namespace Runic{
	template <typename T>
	using Ref = std::shared_ptr<T> ;
	template<typename T, typename ... Args>
	constexpr Ref<T> CreateRef(Args&& ... args)
	{
		return std::make_shared<T>(std::forward<Args>(args)...);
	}

	template <typename T>
	using WeakRef = std::weak_ptr<T>;


	template <typename T>
	using Scope = std::unique_ptr<T>;
	template<typename T, typename ... Args>
	constexpr Scope<T> CreateScope(Args&& ... args)
	{
		return std::make_unique<T>(std::forward<Args>(args)...);
	}



}