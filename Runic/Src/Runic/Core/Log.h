#pragma once
#include "Core.h"
#include "spdlog/spdlog.h"
#include "spdlog/sinks/stdout_color_sinks.h"
#include "spdlog/fmt/ostr.h"
#include "Runic/Utilities.h"
namespace Runic {

	class RUNIC_API Log
	{

	public:
		static void Init();
		inline static std::shared_ptr<spdlog::logger>& GetCoreLogger() { return s_CoreLogger; };
		inline static std::shared_ptr<spdlog::logger>& GetClientLogger() { return s_ClientLogger; };
		inline static std::shared_ptr<LogBuffer>& GetBuffer() { return s_Buffer; };
	private:


		static std::shared_ptr<spdlog::logger> s_CoreLogger;
		static std::shared_ptr<spdlog::logger> s_ClientLogger;
		static std::shared_ptr<LogBuffer> s_Buffer;
	};
}
// Core Log Macros
#define RUNIC_CORE_TRACE(...)	::Runic::Log::GetCoreLogger()->trace(__VA_ARGS__)
#define RUNIC_CORE_INFO(...)	::Runic::Log::GetCoreLogger()->info(__VA_ARGS__)
#define RUNIC_CORE_WARN(...)	::Runic::Log::GetCoreLogger()->warn(__VA_ARGS__)
#define RUNIC_CORE_ERROR(...)	::Runic::Log::GetCoreLogger()->error(__VA_ARGS__)
#define RUNIC_CORE_FATAL(...)	::Runic::Log::GetCoreLogger()->critical(__VA_ARGS__)
// Client Log Macros			
#define RUNIC_TRACE(...)		::Runic::Log::GetClientLogger()->trace(__VA_ARGS__)
#define RUNIC_INFO(...)			::Runic::Log::GetClientLogger()->info(__VA_ARGS__)
#define RUNIC_WARN(...)			::Runic::Log::GetClientLogger()->warn(__VA_ARGS__)
#define RUNIC_ERROR(...)		::Runic::Log::GetClientLogger()->error(__VA_ARGS__)
#define RUNIC_FATAL(...)		::Runic::Log::GetClientLogger()->fatal(__VA_ARGS__)