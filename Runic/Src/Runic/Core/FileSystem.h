#pragma once
#include <filesystem>

namespace Runic {
	using Path = std::filesystem::path;
	using DirectoryIterator = std::filesystem::directory_iterator;
	using DirectoryIteratorRecursive = std::filesystem::recursive_directory_iterator;

	class FileSystem {
	public:
		static bool CreateDirectory(Path path) { return std::filesystem::create_directory(path); }
		static bool IsDirectory(Path path) { return std::filesystem::is_directory(path); }
		static bool Exists(Path path) { return std::filesystem::exists(path); }
		static Path GetWorkingDirectory() { return s_Data->WorkingDirectory;};
		static void SetWorkingDirectory(Path p) {
			s_Data->WorkingDirectory = p;
		};

		static uint32_t GetPathLength(const Path& path) {
			uint32_t i = 0;
			for (auto entry : path) {
				i++;
			}
			return i;
		}

		static time_t GetLastWriteTime(Path path) {
			return to_time_t(std::filesystem::last_write_time(path));
		}
		static size_t HashValue(Path path) { return std::filesystem::hash_value(path); }
	private:
		template <typename TP>
		static std::time_t to_time_t(TP tp)
		{
			using namespace std::chrono;
			auto sctp = time_point_cast<system_clock::duration>(tp - TP::clock::now()
				+ system_clock::now());
			return system_clock::to_time_t(sctp);
		}
	private:
		struct FileSystemData {
			Path WorkingDirectory;

		};

		static Scope<FileSystemData> s_Data;
	};
}