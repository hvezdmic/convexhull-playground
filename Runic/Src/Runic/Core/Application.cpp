#include "runicpch.h"
#include "Runic/Core/Application.h"
#include "Runic/Events/Event.h"
#include "Runic/Core/Log.h"
#include "Runic/Input.h"
#include "glad/glad.h"
#include <GLFW/glfw3.h>

namespace Runic {


	Application* Application::s_Instance = nullptr;

	Application::Application(const std::string& title)
	{

		RUNIC_CORE_ASSERT(!s_Instance, "There has to be only one Application!");
		s_Instance = this;
		m_Window = std::unique_ptr<Window>(Window::Create(WindowProps(title, 800, 800)));
		m_Window->SetEventCallback(RUNIC_BIND_EVENT_FN(Application::OnEvent));
		

		m_ImGuiLayer = new ImGuiLayer();
		PushOverlay(m_ImGuiLayer);
	}

	Application::~Application()
	{
	}
	void Application::Run()
	{
		while (m_Running) {
			float  time = glfwGetTime();
			DeltaTime ts = time - m_LastTime;

			m_LastTime = time;
			for (Layer* layer : m_LayerStack)
				layer->OnUpdate(ts);

			m_ImGuiLayer->Begin();
			for (Layer* layer : m_LayerStack)
				layer->OnImGuiRender();
			m_ImGuiLayer->End();
			
			m_Window->OnUpdate(ts);
		}

	}
	void Application::OnEvent(Event & e)
	{
		EventDispatcher dispatcher(e);
		dispatcher.Dispatch<WindowCloseEvent>(RUNIC_BIND_EVENT_FN(Application::OnWindowClose));
		dispatcher.Dispatch<WindowResizeEvent>(RUNIC_BIND_EVENT_FN(Application::OnWindowResize));

		for (auto it = m_LayerStack.end(); it != m_LayerStack.begin();) {
			(*--it)->OnEvent(e);
			if (e.Handled) break;
		}

	}
	void Application::Close()
	{
		m_Running = false;
	}
	void Application::PushLayer(Layer * layer)
	{
		m_LayerStack.PushLayer(layer);
		layer->OnAttach();
	}
	void Application::PushOverlay(Layer * overlay)
	{
		m_LayerStack.PushOverlay(overlay);
		overlay->OnAttach();
	}
	bool Application::OnWindowClose(WindowCloseEvent & e)
	{
		m_Running = false;
		return true;
	}
	bool Application::OnWindowResize(WindowResizeEvent& e)
	{
		if (e.GetWidth() == 0 || e.GetHeight() == 0) {
			m_Minimized = true;
			return false;
		}
		m_Minimized = false;

		return false;
	}
}
