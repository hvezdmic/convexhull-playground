#pragma once
#ifdef RUNIC_PLATFORM_WINDOWS
	extern Runic::Application* Runic::CreateApplication(int argc, char* argv[]);


int main(int argc, char* argv[]) {
	Runic::Log::Init();
	auto app = Runic::CreateApplication(argc, argv);
	if (app) {
		app->Run();
		delete app;
	}
	return 0;
}
#endif // RUNIC_PLATFORM_WINDOWS