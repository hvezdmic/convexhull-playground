#pragma once
#include "Runic/Core/Core.h"
#include "Runic/Core/Window.h"
#include "Runic/Events/Event.h"
#include "Runic/Events/ApplicationEvent.h"
#include "Runic/Core/LayerStack.h"
#include "Runic/Core/TimeStep.h"
#include "Runic/ImGui/ImGuiLayer.h"


namespace Runic {
	class RUNIC_API Application
	{
	public:
		Application(const std::string& title);
		virtual ~Application();
		void Run();
		void OnEvent(Event& e);
		void Close();
		void PushLayer(Layer* layer);
		void PushOverlay(Layer* overlay);
		inline static Application& Get() { return *s_Instance; }
		inline Window& GetWindow() { return *m_Window; };
	private:
		bool OnWindowClose(WindowCloseEvent& e);
		bool OnWindowResize(WindowResizeEvent& e);
	private:
		std::unique_ptr<Window> m_Window;
		ImGuiLayer* m_ImGuiLayer;
		bool m_Running = true;
		bool m_Minimized = false;
		LayerStack m_LayerStack;
		float m_LastTime = 0.0f;
	private:
		static Application* s_Instance;
	};
	//To be defined in a CLIENT
	Application* CreateApplication(int argc, char* argv[]);
}
