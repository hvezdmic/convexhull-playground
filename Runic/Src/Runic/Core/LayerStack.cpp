#include "runicpch.h"
#include "LayerStack.h"

Runic::LayerStack::LayerStack()
{
}

Runic::LayerStack::~LayerStack()
{
	for (Layer* layer : m_Layers)
		delete layer;
	
}

void Runic::LayerStack::PushLayer(Layer * layer)
{
	m_Layers.emplace(m_Layers.begin() + m_LayerInsertIdx, layer);
	m_LayerInsertIdx++;
}

void Runic::LayerStack::PushOverlay(Layer * overlay)
{
	m_Layers.emplace_back(overlay);
}

void Runic::LayerStack::PopLayer(Layer * layer)
{
	auto it = std::find(m_Layers.begin(), m_Layers.end(), layer);
	if (it!= m_Layers.end())
	{
		m_Layers.erase(it);
		m_LayerInsertIdx--;
	}
}

void Runic::LayerStack::PopOverLay(Layer * overlay)
{
	auto it = std::find(m_Layers.begin(), m_Layers.end(), overlay);
	if (it != m_Layers.end())
	{
		m_Layers.erase(it);
	}
}


