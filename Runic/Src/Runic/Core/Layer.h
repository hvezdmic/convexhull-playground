#pragma once
#include "runicpch.h"
#include "Runic/Core/Core.h"
#include "Runic/Events/Event.h"
#include "Runic/Core/TimeStep.h"


namespace Runic {
	class RUNIC_API Layer
	{
	public:
		Layer(const std::string& name = "Layer");
		virtual ~Layer();

		virtual void OnAttach() {}
		virtual void OnDetach() {}
		virtual void OnUpdate(DeltaTime ts) {}
		virtual void OnEvent(Event& e) {}
		virtual void OnImGuiRender() {}
		inline const std::string& GetName() const { return m_DebugName;  }
	private:
		std::string m_DebugName;
	};

}
