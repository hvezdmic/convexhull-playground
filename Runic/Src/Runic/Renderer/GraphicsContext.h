#pragma once

namespace Runic {
	class RUNIC_API GraphicsContext
	{
	public:
		virtual bool Init() = 0;
		virtual void SwapBuffers() = 0;
	};
}