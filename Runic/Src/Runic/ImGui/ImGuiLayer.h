#pragma once

#include "Runic/Core/Layer.h"
#include "Runic/Events/KeyEvent.h"
#include "Runic/Events/MouseEvent.h"
#include "Runic/Events/ApplicationEvent.h"
namespace Runic {
	class RUNIC_API ImGuiLayer : public Layer
	{
	public:
		ImGuiLayer();
		~ImGuiLayer();

		virtual void OnAttach() override;
		virtual void OnDetach() override;
		virtual void OnImGuiRender() override;
		void Begin();
		void End();

	private:
		float m_Time = 0.0f;
	};
}

