#pragma once
#include "Event.h"
#include "Runic/Scene/Scene.h"
#include "Runic/Scene/Entity.h"
namespace Runic {

	class RUNIC_API SceneChangedEvent : public Event {
	public:
		std::string ToString() const override
		{
			std::stringstream ss;
			ss << "Scene Changed: ";
			return ss.str();
		}


		EVENT_CLASS_TYPE(SceneChanged)
		EVENT_CLASS_CATEGORY(EventCategoryScene)
		


	private:
		Ref<Scene> m_ActiveScene;


	};

	class EntityAddedEvent : public Event
	{
	public:
		EntityAddedEvent(const Entity& e) : m_entity(e) {}
		inline Entity GetEntity() const { return m_entity; }

		std::string ToString() const override
		{
			std::stringstream ss;
			ss << "EntityAddedEvent: ";
			return ss.str();
		}

		EVENT_CLASS_TYPE(None)
			EVENT_CLASS_CATEGORY(EventCategoryScene)

	private:
		Entity m_entity;
	};
	class EntityRemovedEvent : public Event
	{
	public:
		EntityRemovedEvent(const Entity& e) : m_entity(e) {}
		inline Entity GetEntity() const { return m_entity; }

		std::string ToString() const override
		{
			std::stringstream ss;
			ss << "EntityRemovedEvent: ";
			return ss.str();
		}

		EVENT_CLASS_TYPE(None)
			EVENT_CLASS_CATEGORY(EventCategoryScene)

	private:
		Entity m_entity;
	};

	class EntityChangedEvent : public Event
	{
	public:
		EntityChangedEvent(const Entity& e) : m_entity(e) {}
		inline Entity GetEntity() const { return m_entity; }

		std::string ToString() const override
		{
			std::stringstream ss;
			ss << "EntityChangedEvent: ";
			return ss.str();
		}

		EVENT_CLASS_TYPE(None)
			EVENT_CLASS_CATEGORY(EventCategoryScene)

	private:
		Entity m_entity;
	};
}