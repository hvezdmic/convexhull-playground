#include "runicpch.h"
#include "Components.h"
#include "Scene.h"
#include "Entity.h"
namespace Runic {




	Scene::Scene()
	{


	}

	void Scene::OnUpdate(DeltaTime ts)
	{

	}
	void Scene::OnSceneStart()
	{
	}
	void Scene::OnSceneStop()
	{
	}
	void Scene::RemoveEntity(Entity e)
	{
		m_Registry.destroy(e.GetHandle());
	}
	Entity Scene::GetEntity(entt::entity handle)
	{
		return m_Registry.valid(handle) ? Entity(handle, this) : Entity();
	}

	Entity Runic::Scene::CreateEntity(std::string name)
	{
		Entity entity = { m_Registry.create(), this };
		entity.AddComponent<TransformComponent>(glm::mat4(1));
		entity.AddComponent<TagComponent>(name.empty() ? "Entity" : name);
		return entity;
	}
}