#pragma once

#include "glm/gtc/quaternion.hpp"
#include "glm/gtx/quaternion.hpp"


namespace Runic {
	class Transform {
	public:
		inline glm::vec3 Position() { return m_Position; }
		inline glm::quat Rotation() { return m_LocalRotation; }
		inline glm::vec3 Euler() { return m_LocalEuler; }
		inline glm::vec3 Scale() { return m_Scale; }

		void SetPosition(glm::vec3 position) { m_Position = position;  RecalculateTransform(); };
		void SetLocalRotation(glm::quat rotation) { m_LocalEuler = glm::eulerAngles(rotation); m_LocalRotation = rotation; RecalculateTransform(); };
		void SetLocalRotation(glm::vec3 euler);
		void SetScale(glm::vec3 scale) { m_Scale = scale; RecalculateTransform(); };

		glm::mat4 Matrix() { return m_Transform; }


		glm::vec3 GetLocalEulerRotation() { return glm::eulerAngles(m_LocalRotation); }


		Transform() = default;
		Transform(const Transform&) = default;
		Transform(const glm::mat4& transform) : m_Transform(transform) {}
		operator glm::quat()& { return m_LocalRotation; };
		operator glm::mat4() { return m_Transform; }
		operator const glm::mat4() const { return m_Transform; }

	private:
		void RecalculateTransform();
	private:
		glm::quat m_LocalRotation = glm::quat(1.0f, 0.0f, 0.f, 0.0f);
		glm::vec3 m_LocalEuler{ 0,0,0 };
		glm::vec3 m_Position = { 0.0f,0.0f,0.0f };
		glm::vec3 m_Scale = { 1.0f,1.0f,1.0f };
		glm::mat4 m_Transform = glm::mat4(1.0f);




	};
}