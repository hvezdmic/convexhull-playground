#pragma once
#include "glm/glm.hpp"
#include "Transform.h"
namespace Runic {


	struct TagComponent {
		std::string Tag;
		bool Active = true;


		TagComponent() = default;
		TagComponent(const TagComponent&) = default;
		TagComponent(const std::string & tag) : Tag(tag) {}

		operator std::string() { return Tag; }
	};


	
	struct TransformComponent {

		Transform transform;
		bool UniformScale = false;
		
		TransformComponent() = default;
		TransformComponent(const TransformComponent&) = default;
		TransformComponent(const glm::mat4& transform) : transform(transform) {}

		operator glm::mat4() { return transform; }
		operator const glm::mat4() const { return transform; }
	};



}