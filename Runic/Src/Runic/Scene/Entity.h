#pragma once

#include "Scene.h"

#include "entt.hpp"

namespace Runic
{
	class Entity {
	public:

		Entity() = default;
		
		Entity(const Entity& entity) = default;
		Entity(entt::entity handle, Scene* scene);
		~Entity();
	
		entt::entity GetHandle() { return m_EntityHandle; }

		template<typename T, typename... Args>
		T& AddComponent(Args&&... args) {
			return m_Scene->m_Registry.emplace<T>(m_EntityHandle, std::forward<Args>(args)...);
		}


		template<typename T, typename... Args>
		T& GetComponent(Args&&... args) {
			return m_Scene->m_Registry.get<T>(m_EntityHandle, std::forward<Args>(args)...);
		}

		template<typename T>
		bool HasComponent() {
			return m_Scene->m_Registry.has<T>(m_EntityHandle);
		}

		template<typename T>
		void RemoveComponent() {
			m_Scene->m_Registry.remove<T>(m_EntityHandle);
		}
		bool IsValid() {
			return m_EntityHandle != entt::null && m_Scene != nullptr && m_Scene->m_Registry.valid(m_EntityHandle);
		}
		operator bool() const { return m_EntityHandle != entt::null && m_Scene != nullptr; }

		operator uint32_t() { return (uint32_t)m_EntityHandle; }
		bool operator==(const Entity& other) {
			return m_EntityHandle == other.m_EntityHandle && m_Scene == other.m_Scene;
		}
		bool operator!=(const Entity& other) {
			return !(*this == other);
		}
	private:
		Scene* m_Scene = nullptr;
		entt::entity m_EntityHandle{ entt::null };
	};
}