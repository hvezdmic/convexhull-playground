#pragma once
#include "Runic.h"
namespace Runic {

	class SceneManager {
	public:
		static Ref<Scene> GetActiveScene() { return s_Data->m_ActiveScene; };
		static void SetActiveScene(const Ref<Scene> scene) { s_Data->m_ActiveScene = scene; };
		struct SceneManagerData {
			Ref<Scene> m_ActiveScene;
		};

		static Scope<SceneManagerData> s_Data;
	private:

	};

}