#pragma once
#include <string>
#include "entt.hpp"
#include "Runic/Core/TimeStep.h"
namespace Runic {

	class Entity;



	class Scene {
	public:
		Scene();
		void OnUpdate(DeltaTime ts);
		void OnSceneStart();
		void OnSceneStop();
		
		template<typename... Component, typename... Exclude>
		entt::basic_view<entt::entity,entt::exclude_t<Exclude...>, Component...> view(entt::exclude_t<Exclude...> excludes = {}) {
			return m_Registry.view<Component...>(std::forward<Exclude>(excludes)...);
		}
		void RemoveEntity(Entity e);
		Entity GetEntity(entt::entity handle);
		Entity CreateEntity(std::string name);
	private:
		friend class Entity;
		entt::registry m_Registry;
	};
}
