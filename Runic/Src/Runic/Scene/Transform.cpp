#include "runicpch.h"
#include "Transform.h"


namespace Runic {
	void Transform::SetLocalRotation(glm::vec3 euler)
	{

		m_LocalEuler = euler;

		glm::quat roll(glm::cos(euler.x * 0.5f), glm::sin(euler.x * 0.5f), 0, 0);
		glm::quat pitch(glm::cos(euler.y * 0.5f), 0, glm::sin(euler.y * 0.5f), 0);
		glm::quat yaw(glm::cos(euler.z * 0.5f), 0, 0, glm::sin(euler.z * 0.5f));

		m_LocalRotation = pitch * yaw * roll;

		RecalculateTransform();
	}

	void Transform::RecalculateTransform()
	{
		glm::mat4 T = glm::translate(glm::mat4(1.0f), m_Position);
		glm::mat4 R = glm::toMat4(m_LocalRotation);
		glm::mat4 S = glm::scale(glm::mat4(1.0f), m_Scale);
		m_Transform = T * R * S;
	}
}