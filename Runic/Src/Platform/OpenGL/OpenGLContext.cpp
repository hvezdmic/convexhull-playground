#include "runicpch.h"
#include "OpenGLContext.h"
#include "GLFW/glfw3.h"
#include <glad/glad.h>

Runic::OpenGLContext::OpenGLContext(GLFWwindow * windowHandle) : m_WindowHandle(windowHandle)
{
}

bool Runic::OpenGLContext::Init()
{
	glfwMakeContextCurrent(m_WindowHandle);
	int status = gladLoadGLLoader((GLADloadproc)glfwGetProcAddress);
	RUNIC_CORE_ASSERT(status, "Glad failed to initialize!");
	RUNIC_CORE_INFO("OpenGL Info:");
	RUNIC_CORE_INFO("  Vendor: {0}", glGetString(GL_VENDOR));
	RUNIC_CORE_INFO("  Renderer: {0}", glGetString(GL_RENDERER));
	RUNIC_CORE_INFO("  Version: {0}", glGetString(GL_VERSION));
	return false;
}

void Runic::OpenGLContext::SwapBuffers()
{


	glfwSwapBuffers(m_WindowHandle);
}


