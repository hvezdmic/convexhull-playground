#pragma once
#include "Runic/Renderer/GraphicsContext.h"
struct GLFWwindow;

namespace Runic {


	class OpenGLContext : public GraphicsContext
	{
	public:
		OpenGLContext(GLFWwindow* windowHandle);
		virtual bool Init() override;
		virtual void SwapBuffers() override;

	private: 
		GLFWwindow* m_WindowHandle;
	};

}

