#pragma once
#include "Runic/Input.h"
#include "runicpch.h"

namespace Runic {
	class WindowsInput : public Input
	{
	protected:
		virtual bool IsKeyPressedImpl(int keycode) override;
		virtual  bool isMouseButtonPressedImpl(int btn) override;
		virtual  std::pair<float, float>  GetMousePosImpl() override;
		virtual  float GetMouseXImpl() override;
		virtual  float GetMouseYImpl() override;
	};
}

