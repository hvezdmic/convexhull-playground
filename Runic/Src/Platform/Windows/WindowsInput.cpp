#include "runicpch.h"
#include "WindowsInput.h"

#include "Runic/Core/Application.h"
#include "GLFW/glfw3.h"


namespace Runic {
	Input* Input::s_Instance = new WindowsInput();

	bool WindowsInput::IsKeyPressedImpl(int keycode)
	{
		auto window = static_cast<GLFWwindow*>(Application::Get().GetWindow().GetNativeWindow());
		auto state = glfwGetKey(window, keycode);
		return state == GLFW_PRESS || state == GLFW_REPEAT;
	}
	bool WindowsInput::isMouseButtonPressedImpl(int btn)
	{
		auto window = static_cast<GLFWwindow*>(Application::Get().GetWindow().GetNativeWindow());
		auto state = glfwGetMouseButton(window, btn);
		return state == GLFW_PRESS;
	}
	std::pair<float, float>  WindowsInput::GetMousePosImpl()
	{
		auto window = static_cast<GLFWwindow*>(Application::Get().GetWindow().GetNativeWindow());

		double xpos, ypos;
		glfwGetCursorPos(window, &xpos, &ypos);
		return std::pair<float, float>((float)xpos, (float)ypos);

	}
	float WindowsInput::GetMouseXImpl()
	{
		auto[x, _] = GetMousePos();
		return x;
	}
	float WindowsInput::GetMouseYImpl()
	{
		auto[_, y] = GetMousePos();
		return y;
	}

}