#pragma once

#include <iostream>
#include <memory>
#include <functional>
#include <utility>
#include "Runic/Core/Log.h"

#include <sstream>
#include <string>
#include <vector>
#include <unordered_map>
#include <unordered_set>

#ifdef RUNIC_PLATFORM_WINDOWS
	#include <Windows.h>
#endif // RUNIC_PLATFORM_WINDOWS
