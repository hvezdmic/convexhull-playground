#pragma once
#include <stdio.h>
#include "Runic/Core/Application.h"
#include "Runic/Core/Log.h"
#include "Runic/Core/Layer.h"

#include "Runic/Core/TimeStep.h"

#include "Runic/Input.h"
#include "Runic/KeyCodes.h"
#include "Runic/MouseButtonCodes.h"

#include "Runic/Utilities.h"

//----------------------------------------
//------- Scene --------------------------
#include "Runic/Scene/Components.h"
#include "Runic/Scene/Entity.h"
#include "Runic/Scene/Scene.h"
#include "Runic/Scene/SceneManager.h"
//----------------------------------------

#include "Runic/ImGui/ImGuiLayer.h"
//-------- File System -------------------
#include "Runic/Core/FileSystem.h"
//----------------------------------------
#include "Runic/Events/SceneEvent.h"